numpy==1.15.3
pillow==6.2.0
pip==10.0.1
tqdm==4.26.0
torch==1.10.0
torchvision==0.2.1