## Generating MNIST with Flows in PyTorch

PyTorch implementation of a [coupling flow model](https://arxiv.org/abs/1605.08803)\.


## Usage

  1. Run `pip install -r requirements.txt` to set up the environment.
  2. Run `python train.py -h` to see options.
  3. Run `python train.py [FLAGS]`.
  4. After each epoch, samples from the model are saved to `samples/epoch_N.png`.
 


